﻿using System;

namespace Noroff_Task_6
{
    class Program
    {
        static void Main(string[] args)
        {
            double bmi = GetBMI();
            printResult(bmi);
        }

        /*Prompts the user for weigth and height, calls the method 
        for calculating bmi, and returs bmi*/
        private static double GetBMI()
        {
            Console.Write("Your weight in whole KG: ");
            int weightKG = Convert.ToInt32(Console.ReadLine());

            Console.Write("Your height in whole CM: ");
            int heightCM = Convert.ToInt32(Console.ReadLine());

            double bmi = calculate(weightKG, heightCM);

            return bmi;
        }

        //Calculates the bmi based on weight and height.
        private static double calculate(int weightKG, int heightCM)
        {
            //Let's make those CM into M and add power of 2. 
            double heightCalculated = Math.Pow(heightCM / 100.0, 2);
            double bmi = weightKG / heightCalculated;

            return bmi;
        }

        //Prints the result of the bmi-calculation.
        private static void printResult(double bmi)
        {
            if (bmi < 18.5)
            {
                Console.WriteLine("You are underweight, go eat some cake.");
            } else if (bmi < 24.9) {
                Console.WriteLine("You are normal weight, keep it up!");
            } else if (bmi < 29.9)
            {
                Console.WriteLine("You are overweight, better install Beat Saber and get crackin'");
            } else
            {
                Console.WriteLine("The cake is a lie, the cake is a lie, the cakSTOP EATING CAKE! (PS: you are obese)");
            }

        }
    }
}
