# Noroff Task 6 AND 8

Task 6: BMI Calculator
� Write a program that allows
the user to input two values:
� Weight & Height
� It must calculate BMI and then
categorize the result:
� Underweight: BMI is less than 18.5
� Normal weight: BMI is 18.5 to 24.9
� Overweight: BMI is 25 to 29.9
� Obese: BMI is 30 or more

Task 8: Upgrades
� Modify your BMI calculator solution to utilized methods